package com.gitlab.davinkevin.issues.spring.sse.testinfinitessewithmvc.handler

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.http.codec.ServerSentEvent
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.returnResult
import reactor.test.StepVerifier

@WebMvcTest
@Import(SSEConfig::class)
class HandlerTest(
    @Autowired val rest: WebTestClient
) {

    @Test
    fun `should receive one message`() {
        /* Given */
        /* When */
        StepVerifier.create(
            rest
                .get()
                .uri("/is/ok")
                .accept(MediaType.TEXT_EVENT_STREAM)
                /* Then */
                .exchange()
                .expectStatus()
                .isOk
                .returnResult<ServerSentEvent<Int>>()
                .responseBody
        )
            .expectSubscription()
            .assertNext {
                assertThat(it.event()).isEqualTo("heartbeat")
                assertThat(it.data()).isEqualTo(1)
            }
            .verifyComplete()
    }

    @Test
    fun `should receive infinite number of message and just take the first two`() {
        /* Given */
        /* When */
        StepVerifier.create(
            rest
                .get()
                .uri("/is/infinite")
                .accept(MediaType.TEXT_EVENT_STREAM)
                /* Then */
                .exchange()
                .expectStatus()
                .isOk
                .returnResult<ServerSentEvent<Int>>()
                .responseBody
                .take(2)
        )
            .expectSubscription()
            .assertNext {
                assertThat(it.event()).isEqualTo("heartbeat")
                assertThat(it.data()).isEqualTo(1)
            }
            .verifyComplete()
    }



}