package com.gitlab.davinkevin.issues.spring.sse.testinfinitessewithmvc.handler

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.router
import reactor.core.publisher.Flux
import java.time.Duration.ZERO
import java.time.Duration.ofSeconds

@Configuration
class SSEConfig {

    @Bean
    fun messageRouter() = router {

        GET("/is/ok") {
            ServerResponse.sse { it.apply {
                event("heartbeat")
                send(1)
                complete()
            } }
        }

        GET("/is/infinite") {
            ServerResponse.sse { sse ->
                Flux.interval(ZERO, ofSeconds(1))
                    .subscribe {
                        sse.event("heartbeat")
                            .send(it)
                    }
            }
        }
    }
}