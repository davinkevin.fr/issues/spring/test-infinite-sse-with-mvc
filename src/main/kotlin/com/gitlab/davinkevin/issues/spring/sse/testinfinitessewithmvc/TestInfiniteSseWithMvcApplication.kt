package com.gitlab.davinkevin.issues.spring.sse.testinfinitessewithmvc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestInfiniteSseWithMvcApplication

fun main(args: Array<String>) {
	runApplication<TestInfiniteSseWithMvcApplication>(*args)
}